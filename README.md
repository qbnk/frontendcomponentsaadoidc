A part of the qbnk/frontend-components package.  
This package contains an Azure AD Open ID connect apdater, which is used
to log the user in via Azure AD, using the Open ID Connect hybrid flow.

Hybrid flow works a bit different than the normal authentication flows, since it does not require an access token. Instead
an ID token is fetched, and the user is verified via that token. This means that anyone with a Microsoft account can
possibly log in to the site, and it is therefore crucial that there is a defined list of tenant IDs that the user can
belong to in order for the login to function.

By using the hybrid flow, there is no need for a client secret, only client ID. Because of this, no access token can be fetched.

**In Azure AD, a return URL (i.e. where the id token is sent after a successful login) must be defined.  
This is set automatically by the adapter, and will be the sites URL followed by `/oidc/token_verification`**

Define the adapter in your dependencies

Frontendcomponentsaadoidc now supports Open ID Connect b2c as of version 1.0.7. Specify the needed values in your config file. 

```php
use QBNK\FrontendComponents\Auth\Adapter\OIDC;

$container[QB_AUTH_OIDC] = function(ContainerInterface $container) use ($app) {
    $settings = $container->get(QB_SETTINGS)[QB_AUTH_OIDC];
    return new OIDC($app, $settings);
};
```

Don't forget to register the routes in your `routes.php` file.

```php
$app->getContainer()->get(QB_AUTH_OIDC)->registerRoutes($app);
```

Sample settings
```php
$settings = [
    'allowedTenants' => [
        'tenant 1',
        'tenant 2',
    ],
    'allowedIssuer' => ['https://mediaportal.qbank.se'],
    'allowedAudience' => ['audience 1'],
    'clientId' => !empty($_ENV['QBANK_API_CLIENT_ID']) ? $_ENV['QBANK_API_CLIENT_ID'] : '',
    'allowExternalUsers' => true,
    'internalEmailAddresses' => ['customer.com'],
    'groupMapping' => [
        'defaultInternalUserGroups' => ['Group name' => 1],
        'defaultExternalUserGroups' => ['Group name' => 2],
    ],
    'b2c' => true,
    'p' => 'B2C_1A_SIGNUP_SIGNIN',
    'scopes' => ['openid'],
    'tenant' => 'tenantName',
    'pathAuthorize' => "/oauth2/v2.0/authorize",
    'pathToken' => "/oauth2/v2.0/token",
    'QBankUserLookup' => true, // Will try to find a user with the OIDC username in QBank, and add the user to the Identity Session
    'urlLogin' => 'https://customer.b2clogin.com/', // if using 2bc
    'defaultAlgorithm' => '', // if using 2bc
    'allowedTenants' => ['Tenant ID'],
    'usernameClaim' => 'email', // set username from claims
    'allowedIssuer' => ['customer login url'],
    'allowedAudience' => [],
    'QBankUserLookup' => true,
    'includeUserDataCookie' => true // When session is stored in cookie
];
```
When performing a login, call the adapater's authenticate method.
```php
/** @var \QBNK\FrontendComponents\Auth\Adapter\OIDC $oidc */
$oidc = $this->app->getContainer()->get(QB_AUTH_OIDC);
$returnTo = 'return url';
$params = []; // Ignored
$oidc->authenticate($returnTo, $params);
```

Logging the user out is just unsetting the current session. Redirect the user to the logout route
```php
$this->app->getContainer()->get(QB_ROUTER)->pathFor(OIDC::ROUTE_LOGOUT);
```

**NB:** This package currently does not support creating QBank users.