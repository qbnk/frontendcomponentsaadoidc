<?php

namespace QBNK\FrontendComponents\Auth\Adapter;

use Exception;
use Firebase\JWT\JWT;
use QBNK\FrontendComponents\Auth\Identity;
use QBNK\QBank\API\QBankApi;
use QBNK\QBank\API\CachePolicy;
use QBNK\QBank\API\Exception\QBankApiException;
use Slim\App;
use Slim\Router;
use SlimSession\Helper;
use TheNetworg\OAuth2\Client\Provider\Azure;

class OIDC implements AdapterInterface
{
    public const ROUTE_LOGOUT       = 'oidc_logout';
    public const ROUTE_VERIFY_TOKEN = 'oidc_verify_token';
    /**
     * @var App
     */
    private $app;
    /**
     * @var array
     */
    private $settings;
    /**
     * @var Azure $provider
     */
    private $provider;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var string
     */
    private $baseUrl;
    /**
     * @var QBankApi
     */
    private $qbankApi;
    /**
     * @var Helper
     */
    private $session;
    /**
     * @var boolean
     */
    private $isInternal;
    /**
     * @var boolean
     */
    private $isExternal;

    /**
     * @throws Exception
     */
    public function __construct(App $app, array $settings = [])
    {
        $this->app = $app;
        $this->settings = $settings;
        $this->verifySettings();
        $this->session = $this->app->getContainer()->get(QB_SESSION);
        $this->qbankApi = $this->app->getContainer()->get(QB_QBANKAPI);
        $this->router = $this->app->getContainer()->get(QB_ROUTER);
        $this->baseUrl = $this->app->getContainer()->get('request')->getUri()->getBaseUrl();
    }

    /**
     * @throws Exception
     */
    private function verifySettings(): void
    {
        if (
            !isset($this->settings['allowedTenants'])
            || !is_array($this->settings['allowedTenants'])
            || count($this->settings['allowedTenants']) <= 0
        ) {
            die('There must be at least one allowed tenant');
        }
        if (
            !isset($this->settings['clientId'])
            || !is_string($this->settings['clientId'])
            || empty($this->settings['clientId'])
        ) {
            die('Client ID must be defined!');
        }
    }

    private function setProvider(): void
    {
        $redirectUri = $this->baseUrl . $this->router->pathFor(self::ROUTE_VERIFY_TOKEN);
        $this->provider = new Azure([
            'clientId' => $this->settings['clientId'],
            'redirectUri' => $redirectUri,
            'scopes' => $this->settings['scopes'],
            'defaultEndPointVersion' => '2.0',
            'pathAuthorize' => $this->settings['pathAuthorize'],
            'pathToken' => $this->settings['pathToken'],
            'tenant' => $this->settings['tenant'],
            'defaultAlgorithm' => $this->settings['defaultAlgorithm']
        ]);
        $this->provider->{'urlLogin'} = $this->settings['urlLogin'];
    }

    /**
     * @param string $returnTo Path to redirect to after a successful login
     * @param array  $params   Currently ignored
     *
     * @return void
     */
    public function authenticate($returnTo = null, $params = []): void
    {
        $this->session->set('oidc_return_to', $this->getReturnToUrl($returnTo));
        $this->login();
    }

    /**
     * @param $returnToUrl
     *
     * @return string
     */
    private function getReturnToUrl($returnToUrl): string
    {
        if ($returnToUrl) {
            if (isset($returnToUrl[0]) && $returnToUrl[0] === '/') {
                $returnToUrl = substr($returnToUrl, 1);
            }
            if ($returnToUrl === '') {
                return $this->baseUrl;
            }
            return $this->baseUrl . '/' . urldecode($returnToUrl);
        }
        return $this->baseUrl;
    }

    /**
     * Fetches provider information and redirects the user to Microsoft for login
     */
    private function login(): void
    {
        $this->setProvider();
        $nonce = sha1(uniqid('qbnkopenidconnect', true));

        $authorizationPayload = [
            'response_type' => 'code id_token',
            'response_mode' => 'form_post',
            'client_id' => $this->settings['clientId'],
            'scope' => $this->provider->scope,
            'nonce' => $nonce,
        ];
        if ($this->settings['b2c'] && !empty($this->settings['p'])) {
            $authorizationPayload['p'] = $this->settings['p'];
        }
        $authorizationUrl = $this->provider->getAuthorizationUrl($authorizationPayload);
        $this->session->set('OAuth2.state', $this->provider->getState());
        $this->session->set('oidc_nonce', $nonce);
        header('Location: ' . $authorizationUrl);
        die();
    }

    /**
     * @return bool
     */
    public function isAuthenticated(): bool
    {
        return ($this->getIdentity() instanceof Identity);
    }

    /**
     * @return Identity|null
     */
    public function getIdentity(): ?Identity
    {
        if (
            $this->session->exists('identity')
            && ($identity = unserialize($this->session->get('identity'), ['allowed_classes' => [Identity::class]])) !== false
        ) {
            return $identity;
        }
        return null;
    }

    /**
     * @return void
     */
    public function logout(): void
    {
        $this->session->delete('identity');
        header('Location: ' . $this->baseUrl);
        die();
    }

    /**
     * Verifies an ID token sent from Azure AD
     *
     * @throws Exception
     */
    public function tokenVerification(): void
    {
        $this->setProvider();

        $error = $_REQUEST['error'];
        if ($error) {
            $errorDescription = $_REQUEST['error_description'];
            die($error . ': ' . $errorDescription);
        }

        $idToken = $_REQUEST['id_token'] ?? null;
        $code = $_REQUEST['code'] ?? null;
        $state = $_REQUEST['state'] ?? null;
        $sessionState = $_REQUEST['session_state'] ?? null;
        $savedState = $this->session->get('OAuth2.state');

        if ($code === null) {
            die('Code missing');
        }
        if ($state === null) {
            die('State missing');
        }
        if ($savedState === null) {
            die('Saved state missing');
        }
        if ($savedState !== $state) {
            die('Invalid state');
        }

        $this->session->delete('OAuth2.state');

        $token_split = explode('.', $idToken);
        $header = json_decode(base64_decode($token_split[0]), true);
        $algorithm = strtoupper($header['alg']) ?? $this->settings['defaultAlgorithm'];
        $keyId = $header['kid'];
        $verificationKey = $this->provider->getJwtVerificationKeys()[$keyId];

        $jwt = (array)JWT::decode($idToken, $verificationKey, [$algorithm]);
        $this->verifyJWT($jwt);
        $this->setIdentityFromJWT($jwt);

        if (!empty($jwt[$this->settings['usernameClaim']])) {
            $userName = $jwt[$this->settings['usernameClaim']];
        } else {
            $userName = 'User';
        }

        $returnTo = $this->session->get('oidc_return_to', $this->baseUrl);
        $this->session->delete('oidc_return_to');

        if (!empty($this->settings['includeUserDataCookie'])) { // For them SPA's
            $sessionSettings = $this->app->getContainer()->get(QB_SETTINGS)[QB_SESSION] ?? [];
            $cookieLifetime = $sessionSettings['lifetime'] ?? '1 hour';
            $displayName = !empty($firstName) && !empty($lastName) ? $firstName[0] . ' ' . $lastName[0] : $userName ?? '';
            setcookie(defined('QB_USER_COOKIE_KEY') ? QB_USER_COOKIE_KEY : 'user-data', json_encode(['username' => $userName, 'displayName' => $displayName]), strtotime($cookieLifetime), '/');
        }

        header('Location: ' . $returnTo);
        die();
    }

    /**
     * @throws Exception
     */
    private function verifyJWT(array $jwt): void
    {
        $nonce = $jwt['nonce'] ?? null;
        $sessionNonce = $this->session->get('oidc_nonce');
        $expirationDate = $jwt['exp'] ?? null;

        if ($nonce === null || $sessionNonce === null) {
            die('Nonce missing');
        }
        if ($expirationDate === null) {
            die('Expiration date missing');
        }
        if ($nonce !== $sessionNonce) {
            die('Nonce mismatch');
        }
        $this->session->delete('oidc_nonce');

        if (time() > $expirationDate) {
            die('Token has expired');
        }


        // Verify Audience ('aud')
        $audience = $jwt['aud'] ?? null;
        if ($audience === null) {
            die('Audience is missing');
        }

        if (!in_array($audience, $this->settings['allowedAudience'], true)) {
            die("Application not allowed, $audience");
        }

        // Verify Issuer ('iss') 
        $issuer = $jwt['iss'] ?? null;
        if ($issuer === null) {
            die('Issuer is missing');
        }

        if (!in_array($issuer, $this->settings['allowedIssuer'], true)) {
            die("Incorrect issuer, $issuer");
        }

        // Verify Tenant ID ('tid') 
        $tenantId = $jwt['tid'] ?? null;
        if ($tenantId === null) {
            die('Tenant ID is missing');
        }

        if (!in_array($tenantId, $this->settings['allowedTenants'], true)) {
            die("User not from allowed tenant, $tenantId");
        }


        if (!empty($this->settings['internalEmailAddresses']) && !empty($jwt['email']) && filter_var($jwt['email'], FILTER_VALIDATE_EMAIL)) {
            $email = $jwt['email'];
            $domain = explode('@', $email)[1];

            if (in_array($domain, $this->settings['internalEmailAddresses'])) {
                $this->isInternal = true;
            } else if ($this->settings['allowExternalUsers']) {
                $this->isExternal = true;
            } else {
                die('External users are not allowed');
            }
        }
    }

    private function setIdentityFromJWT(array $jwt): void
    {
        $userName = $jwt['preferred_username'];
        $identity = new Identity($userName, []);
        $qbankUser = null;
        $shouldLookupQBankUser = !empty($this->settings['QBankUserLookup']);

        if ($this->isInternal && !empty($this->settings['groupMapping']['defaultInternalUserGroups'])) {
            foreach ($this->settings['groupMapping']['defaultInternalUserGroups'] as $groupName => $groupId) {
                $identity->addGroup($groupName, $groupId);
            }
        } else if ($this->isExternal && !empty($this->settings['groupMapping']['defaultExternalUserGroups'])) {
            foreach ($this->settings['groupMapping']['defaultExternalUserGroups'] as $groupName => $groupId) {
                $identity->addGroup($groupName, $groupId);
            }
        }

        if ($shouldLookupQBankUser) {
            try {
                $qbankUser = $this->qbankApi->accounts()->retrieveUser($userName, new CachePolicy(false, 0));
                if ($qbankUser) {
                    $identity->setAttributes(['user' => $qbankUser]);
                    $identity->setFirstName($qbankUser->getFirstName());
                    $identity->setLastName($qbankUser->getLastName());
                    $identity->setQBankId($qbankUser->getId());
                    foreach ($qbankUser->getGroups() as $group) {
                        $identity->addGroup($group->getName(), $group->getId());
                    }
                }
            } catch (QBankApiException $e) {
            }
        }

        $identity->setQBankSessionId(
            $this->qbankApi->events()->session(
                $this->app->getContainer()->get(QB_SETTINGS)[QB_QBANKAPI]['sourceId'],
                uniqid($_SERVER['SERVER_NAME'], false),
                $_SERVER['REMOTE_ADDR'],
                $_SERVER['HTTP_USER_AGENT'],
                !empty($qbankUser) ? $qbankUser->getId() : null
            )
        );

        $this->session->set('identity', serialize($identity));
    }

    /**
     * @return void
     */
    public function registerRoutes(): void
    {
        $me = $this;
        $this->app->group('/oidc', function () use ($me) {
            /** @var App $this */
            $this->post('/token_verification', [$me, 'tokenVerification'])->setName(self::ROUTE_VERIFY_TOKEN);
            $this->get('/logout', [$me, 'logout'])->setName(self::ROUTE_LOGOUT);
        });
    }
}
